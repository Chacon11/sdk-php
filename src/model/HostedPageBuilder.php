<?php

namespace payabl\model;
use payabl\model\HostedPage;

class HostedPageBuilder {

    //Parameters
    public $amount;
    public $currency;
    public $payment_method;
    public $orderid;
    public $firstname;
    public $lastname;
    public $street;
    public $city;
    public $state;
    public $zip;
    public $country;
    public $email;
    public $recurring_id;
    public $notification_url;
    public $external_id;
    public $salutation;
    public $title;
    public $gender;
    public $phone;
    public $fax;
    public $mobile;
    public $company;
    public $custom1;
    public $custom2;
    public $custom3;
    public $birthday;
    public $house;
    public $customerip;
    public $language;

    public $url_return;
    public $url_failed;
    public $url_success;

    public function amount(string $amount): self {
        $this->amount = $amount;
        return $this;
    }

    public function currency(string $currency): self {
        $this->currency = $currency;
        return $this;
    }

    public function orderId(string $orderid): self {
        $this->orderid = $orderid; 
        return $this;
    }

    public function paymentMethod(string $payment_method): self {
        $this->payment_method = $payment_method;
        return $this;
    }

    public function firstName(string $firstname): self {
        $this->firstname = $firstname;
        return $this;
    }

    public function lastName(string $lastname): self {
        $this->lastname = $lastname;
        return $this;
    }

    public function street(string $street): self {
        $this->street = $street;
        return $this;
    }

    public function zip(string $zip): self {
        $this->zip = $zip;
        return $this;
    }

    public function country(string $country): self {
        $this->country = $country;
        return $this;
    }

    public function email(string $email): self {
        $this->email = $email; 
        return $this;
    }

    public function recurringId(string $recurring_id): self {
        $this->recurring_id = $recurring_id;
        return $this;
    }

    public function notificationUrl(string $notification_url): self {
        $this->notification_url = $notification_url;
        return $this;
    }

    public function externalId(string $external_id): self {
        $this->external_id = $external_id;
        return $this;
    }

    public function state(string $state): self {
        $this->state = $state;
        return $this;
    }

    public function city(string $city): self {
        $this->city = $city;
        return $this;
    }

    public function salutation(string $salutation): self {
        $this->salutation = $salutation;
        return $this;
    }

    public function title(string $title): self {
        $this->title = $title;
        return $this;
    }

    public function gender(string $gender): self {
        $this->gender = $gender;
        return $this;
    }

    public function phone(string $phone): self {
        $this->phone = $phone;
        return $this;
    }

    public function fax(string $fax): self {
        $this->fax = $fax;
        return $this;
    }

    public function mobile(string $mobile): self {
        $this->mobile = $mobile;
        return $this;
    }

    public function company(string $company): self {
        $this->company = $company;
        return $this;
    }

    public function custom1(string $custom1): self {
        $this->custom1 = $custom1;
        return $this;
    }

    public function custom2(string $custom2): self {
        $this->custom2 = $custom2;
        return $this;
    }

    public function custom3(string $custom3): self {
        $this->custom3 = $custom3;
        return $this;
    }

    public function birthday(string $birthday): self {
        $this->birthday = $birthday;
        return $this;
    }

    public function house(string $house): self {
        $this->house = $house;
        return $this;
    }

    public function customerIP(string $customerip): self {
        $this->customerip = $customerip;
        return $this;
    }
    
    public function language(string $language): self {
        $this->language = $language;
        return $this;
    }

    public function urlReturn(string $url_return): self {
        $this->url_return = $url_return;
        return $this;
    }

    public function urlSuccess(string $url_success): self {
        $this->url_success = $url_success;
        return $this;
    }

    public function urlFailed(string $url_failed): self {
        $this->url_failed = $url_failed;
        return $this;
    }

    public function build(){

        $result = $this->validationHostedPayment();

        if(!empty($result)){
            return ($result);
        }

        return new HostedPage(
            $this->amount,
            $this->currency,
            $this->orderid,
            $this->payment_method,
            $this->firstname,
            $this->lastname,
            $this->street,
            $this->city,
            $this->state,
            $this->zip,
            $this->country,
            $this->email,
            $this->recurring_id,
            $this->notification_url,
            $this->external_id,
            $this->salutation,
            $this->title,
            $this->gender,
            $this->phone,
            $this->fax,
            $this->mobile,
            $this->company,
            $this->custom1,
            $this->custom2,
            $this->custom3,
            $this->birthday,
            $this->house,
            $this->customerip,
            $this->language,

            $this->url_return,
            $this->url_failed,
            $this->url_success
        
        );
    }


    public function validationHostedPayment(){

        $arr_errors['errors'] = [];

        if (empty($this->amount)) {
            $message = "Failed to pass el amount parameter";
            $arr_errors['errors']['amount'] = $message;
        }

        if (empty($this->currency)) {
            $message = "Failed to pass currency parameter";
            $arr_errors['errors']['currency'] = $message;
        }

        return $arr_errors;

    }
    
}

?>