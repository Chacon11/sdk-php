<?php

namespace payabl\model;

class CreditCard {
    public $amount;
    public $currency;
    public $payment_method;
    public $ccn;
    public $exp_month;
    public $exp_year;
    public $cvc_code;
    public $orderid;
    public $cardholder_name;
    public $firstname;
    public $lastname;
    public $street;
    public $city;
    public $state;
    public $zip;
    public $country;
    public $email;
    public $recurring_id;
    public $notification_url;
    public $external_id;
    public $salutation;
    public $title;
    public $gender;
    public $phone;
    public $fax;
    public $mobile;
    public $company;
    public $custom1;
    public $custom2;
    public $custom3;
    public $birthday;
    public $house;
    public $customerip;
    public $language;
    
    public $url_return;
    public $url_failed;
    public $url_success;
    

    public function __construct(
        string $amount = null,
        string $currency = null,
        string $payment_method = null,
        string $ccn = null,
        string $exp_month = null,
        string $exp_year = null,
        string $cvc_code = null,
        
        string $orderid = null,
        string $cardholder_name = null,
        string $firstname = null,
        string $lastname = null,
        string $street = null,
        string $city = null,
        string $state = null,
        string $zip = null,
        string $country = null,
        string $email = null,
        string $recurring_id = null,
        string $notification_url = null,
        string $external_id = null,
        string $salutation = null,
        string $title = null,
        string $gender = null,
        string $phone = null,
        string $fax = null,
        string $mobile = null,
        string $company = null,
        string $custom1 = null,
        string $custom2 = null,
        string $custom3 = null,
        string $birthday = null,
        string $house = null,
        string $customerip = null,
        string $language = null,

        string $url_return = null,
        string $url_failed = null,
        string $url_success = null
    ){
        $this->amount = $amount;
        $this->currency = $currency;
        $this->payment_method = $payment_method;
        $this->ccn = $ccn;
        $this->exp_month = $exp_month;
        $this->exp_year = $exp_year;
        $this->cvc_code = $cvc_code;
        $this->orderid = $orderid; 
        $this->cardholder_name = $cardholder_name;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->lastname = $lastname;
        $this->zip = $zip;
        $this->state = $state;
        $this->city = $city;
        $this->street = $street;
        $this->country = $country;
        $this->email = $email; 
        $this->recurring_id = $recurring_id;
        $this->notification_url = $notification_url;
        $this->external_id = $external_id;
        $this->salutation = $salutation;
        $this->title = $title;
        $this->gender = $gender;
        $this->phone = $phone;
        $this->fax = $fax;
        $this->mobile = $mobile;
        $this->company = $company;
        $this->custom1 = $custom1;
        $this->custom2 = $custom2;
        $this->custom3 = $custom3;
        $this->birthday = $birthday;
        $this->house = $house;
        $this->customerip = $customerip;
        $this->language = $language;

        $this->url_return = $url_return;
        $this->url_failed = $url_failed;
        $this->url_success = $url_success;
    }

    public static function create(
        string $amount, 
        string $currency, 
        string $payment_method, 
        string $ccn,
        string $exp_month, 
        string $exp_year, 
        string $cvc_code,
        string $orderid,
        string $cardholder_name,
        string $firstname,
        string $lastname,
        string $street,
        string $city,
        string $state,
        string $zip,
        string $country,
        string $email,
        string $recurring_id,
        string $notification_url,
        string $external_id,
        string $salutation,
        string $title,
        string $gender,
        string $phone,
        string $fax,
        string $mobile,
        string $company,
        string $custom1,
        string $custom2,
        string $custom3,
        string $birthday,
        string $house,
        string $customerip,
        string $language,

        string $url_return,
        string $url_failed,
        string $url_success
    ){
        return new self
        (
            $amount, 
            $currency, 
            $payment_method,
            $ccn,
            $exp_month,
            $exp_year,
            $cvc_code,
            $orderid,
            $cardholder_name,
            $firstname,
            $lastname,
            $street,
            $city,
            $state,
            $zip,
            $country,
            $email,
            $recurring_id,
            $notification_url,
            $external_id,
            $salutation,
            $title,
            $gender,
            $phone,
            $fax,
            $mobile,
            $company,
            $custom1,
            $custom2,
            $custom3,
            $birthday,
            $house,
            $customerip,
            $language,
            $url_return,
            $url_failed,
            $url_success
        );
    }
}

?>