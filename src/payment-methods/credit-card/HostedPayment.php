<?php

namespace payabl\creditCard;
use payabl\creditCard\Cashier;
use payabl\model\HostedPageBuilder;
use payabl\service\LoggingService;

#####################
##  HostedPayment  ##
#####################

/**
 * Class HostedPayment
 * @package payabl
 */

class HostedPayment {

    public static $url = '/pay/payment/init';

    public $merchantId;
    public $secret;
    public $baseUrl;
    public $response;

    function __construct($merchantId, $secret, $baseUrl){
        $this->merchantId = $merchantId;
        $this->secret = $secret;
        $this->baseUrl = $baseUrl;
    }


    //==============================================================================

    public function start(HostedPageBuilder $params){

        try {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', '----------------------------------------------------------------------------------------------------------------');
            $loggingService->logInfo('2', 'HostedPayment.php / Payment method: credit-card / Function: start / Message: Start of the transaction...');

            $capture_response = Cashier::createHosted($this->merchantId, $this->secret, $this->baseUrl.self::$url, $params);
        
            $data = Cashier::generateResult($capture_response);
            $this->response = json_encode($data);
    
            $loggingService->logInfo('2', 'HostedPayment.php / Payment method: credit-card / Function: start / Result: '.$this->response);
            return $this->response;

        } catch (\Exception $e) {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'HostedPayment.php / Payment method: credit-card / Function: start / Message: '.$e->getMessage());
        
        }
    }

    //==============================================================================
    


}