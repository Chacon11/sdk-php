<?php

namespace payabl\creditCard;
use payabl\creditCard\Cashier;
use payabl\model\CreditCardBuilder;
use payabl\service\LoggingService;

#####################
##  WidgetPayment  ##
#####################

/**
 * Class WidgetPayment
 * @package payabl
 */

class WidgetPayment {

    public static $url = '/pay/payment/get_payment_widget_session';

    public $merchantId;
    public $secret;
    public $baseUrl;
    public $response;

    function __construct($merchantId, $secret, $baseUrl){
        $this->merchantId = $merchantId;
        $this->secret = $secret;
        $this->baseUrl = $baseUrl;
    }


    //==============================================================================

    public function start(CreditCardBuilder $params){

        try {
            
            $loggingService = new LoggingService();
            $loggingService->logInfo('2', '----------------------------------------------------------------------------------------------------------------');
            $loggingService->logInfo('2', 'WidgetPayment.php / Payment method: credit-card / Function: start / Message: Start of the transaction...');

            $capture_response = Cashier::create($this->merchantId, $this->secret, $this->baseUrl.self::$url, $params);
        
            $data = Cashier::generateResult($capture_response);
            $this->response = json_encode($data);
    
            $loggingService->logInfo('2', 'WidgetPayment.php / Payment method: credit-card / Function: start / Result: '.$this->response);
            return $this->response;

        } catch (\Exception $e) {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'WidgetPayment.php / Payment method: credit-card / Function: start / Message: '.$e->getMessage());
        
        }
    }

    //==============================================================================
    


}

?>