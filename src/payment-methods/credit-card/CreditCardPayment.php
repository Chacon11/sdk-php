<?php

namespace payabl\creditCard;
use payabl\creditCard\Cashier;
use payabl\model\CreditCardBuilder;
use payabl\service\LoggingService;

########################
## CreditCardPayment  ##
########################

/**
 * Class CreditCardPayment
 * @package payabl
 */

class CreditCardPayment {

    public static $url_authorize = '/pay/backoffice/payment_authorize';
    public static $url_preAuthorize = '/pay/backoffice/payment_preauthorize';
    public static $url_reversal = '/pay/backoffice/payment_reversal';
    public static $url_refund = '/pay/backoffice/payment_refund';
    public static $url_capture = '/pay/backoffice/payment_capture';
    public static $url_cft = '/pay/backoffice/payment_cft';

    public $merchantId;
    public $secret;
    public $baseUrl;
    public $response;

    function __construct($merchantId, $secret, $baseUrl){
        $this->merchantId = $merchantId;
        $this->secret = $secret;
        $this->baseUrl = $baseUrl;
    }

    //==============================================================================

    public function authorize(CreditCardBuilder $params){

        try {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', '----------------------------------------------------------------------------------------------------------------');
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: authorize / Message: Start of the transaction...');

            $capture_response = Cashier::create($this->merchantId, $this->secret, $this->baseUrl.self::$url_authorize, $params);
        
            $data = Cashier::generateResult($capture_response);
            $this->response = json_encode($data);
   
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: authorize/ Result: '.$this->response);
            return $this->response;

        } catch (\Exception $e) {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: authorize / Message: '.$e->getMessage());
        
        }
    }

    //==============================================================================

    public function preAuthorize(CreditCardBuilder $params){

        try {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', '----------------------------------------------------------------------------------------------------------------');
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: preAuthorize / Message: Start of the transaction...');

            $capture_response = Cashier::create($this->merchantId, $this->secret, $this->baseUrl.self::$url_preAuthorize, $params);
            $data = Cashier::generateResult($capture_response);
            $this->response = json_encode($data);

            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: preAuthorize/ Result: '.$this->response);
            return $this->response;

        } catch (\Exception $e) {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'CreditCard.php / Payment method: credit-card / Function: create / Message: '.$e->getMessage());
        
        }
    }

    //==============================================================================
    
    public function reversal($transactionid, $language){

        try {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', '----------------------------------------------------------------------------------------------------------------');
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: reversal / Message: Start of the transaction...');

            $capture_response = Cashier::createReversal($this->merchantId, $this->secret, $this->baseUrl.self::$url_reversal, $transactionid, $language);
            $data = Cashier::generateResult($capture_response);
            $this->response = json_encode($data);

            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: reversal/ Result: '.$this->response);
            return $this->response;

        } catch (\Exception $e) {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: reversal / Message: '.$e->getMessage());
        
        }
    }

    //==============================================================================

    public function refund($amount, $currency, $transactionid, $language = null, $externalid = null){

        try {
            
            $loggingService = new LoggingService();
            $loggingService->logInfo('2', '----------------------------------------------------------------------------------------------------------------');
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: refund / Message: Start of the transaction...');

            $capture_response = Cashier::createRefund($this->merchantId, $this->secret, $this->baseUrl.self::$url_refund, $amount, $currency, $transactionid, $language, $externalid);
            $data = Cashier::generateResult($capture_response);
            $this->response = json_encode($data);
            
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: refund/ Result: '.$this->response);
            return $this->response;

        } catch (\Exception $e) {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: refund / Message: '.$e->getMessage());
        
        }
    }

    //==============================================================================

    public function capture($transactionid){

        try {
            
            $loggingService = new LoggingService();
            $loggingService->logInfo('2', '----------------------------------------------------------------------------------------------------------------');
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: capture / Message: Start of the transaction...');

            $capture_response = Cashier::createCapture($this->merchantId, $this->secret, $this->baseUrl.self::$url_capture, $transactionid);
            $data = Cashier::generateResult($capture_response);
            $this->response = json_encode($data);
            
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: capture/ Result: '.$this->response);
            return $this->response;

        } catch (\Exception $e) {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: capture / Message: '.$e->getMessage());
        
        }
    }

    //==============================================================================

    public function partialCapture($transactionid, $amount){

        try {
            
            $loggingService = new LoggingService();
            $loggingService->logInfo('2', '----------------------------------------------------------------------------------------------------------------');
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: capture / Message: Start of the transaction...');

            $capture_response = Cashier::createPartialCapture($this->merchantId, $this->secret, $this->baseUrl.self::$url_capture, $transactionid, $amount);
            $data = Cashier::generateResult($capture_response);
            $this->response = json_encode($data);
            
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: capture/ Result: '.$this->response);
            return $this->response;

        } catch (\Exception $e) {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: capture / Message: '.$e->getMessage());
        
        }
    }

    //==============================================================================

    public function cft_transaction($transactionid, $amount, $currency, $paymentMethod=null){

        try {
            $loggingService = new LoggingService();
            $loggingService->logInfo('2', '----------------------------------------------------------------------------------------------------------------');
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: cft_transaction / Message: Start of the transaction...');

            if($paymentMethod===null || empty($paymentMethod) || $paymentMethod===' '){
                $paymentMethod=1;
            }

            $capture_response = Cashier::createCFT_Transaction($this->merchantId, $this->secret, $this->baseUrl.self::$url_cft, $transactionid, $amount, $currency, $paymentMethod);
            $data = Cashier::generateResult($capture_response);
            $this->response = json_encode($data);
            
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: cft_transaction/ Result: '.$this->response);
            return $this->response;

        } catch (\Exception $e) {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: cft_transaction / Message: '.$e->getMessage());
        
        }
    }

    //==============================================================================

    public function cft_creditCard($amount, $currency, $paymentMethod, $cnn, $expMonth, $expYear, $cardholderName){

        try {
            
            $loggingService = new LoggingService();
            $loggingService->logInfo('2', '----------------------------------------------------------------------------------------------------------------');
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: cft_creditCard / Message: Start of the transaction...');

            $capture_response = Cashier::createCFT_CreditCard($this->merchantId, $this->secret, $this->baseUrl.self::$url_cft, $amount, $currency, $paymentMethod, $cnn, $expMonth, $expYear, $cardholderName);
            $data = Cashier::generateResult($capture_response);
            $this->response = json_encode($data);
            
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: cft_creditCard/ Result: '.$this->response);
            return $this->response;

        } catch (\Exception $e) {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'CreditCardPayment.php / Payment method: credit-card / Function: cft_creditCard / Message: '.$e->getMessage());
        
        }
    }

    //==============================================================================


}


?>