<?php

namespace payabl\creditCard;
use payabl\service\LoggingService;
use payabl\model\CreditCardBuilder;
use payabl\model\HostedPageBuilder;
use Exception;

/**
 * Class Cashier
 * @package payabl
 */

class Cashier{

    
    #############################################
    ##  The parameters to be used for requests ##
    #############################################

    public static $test;
    public static $merchantId;
    public static $secret;
    public static $url;
    public static $params;
    public static $signature;
    public static $requestData;
    public static $responseData;
    public static $transactionId;
    public static $language;
    public static $amount;
    public static $currency;
    
    /**
     * @return string The merchant id used for requests.
     */
    private static function getMerchantId()
    {
        return self::$merchantId;
    } 

    /**
     * Sets the merchant id to be used for requests.
     * @param string $merchantId
     */
    private static function setMerchantId($merchantId)
    {
        self::$merchantId = $merchantId;
    }

    /**
     * @return string The secret used for calculating signature requests.
     */
    private static function getSecret()
    {
        return self::$secret;
    } 

    /**
     * Sets the secret to be used for signature.
     * @param string $secret
     */
    private static function setSecret($secret)
    {
        self::$secret = $secret;
    }

    
    /**
     * @return string The url used for requests.
     */
    private static function getUrl()
    {
        return self::$url;
    } 

    /**
     * Sets the url to be used for requests.
     * @param string $url
     */
    private static function setUrl($url)
    {
        self::$url = $url;
    }

    /**
     * @return string The params for the requests.
    */
    private static function getParams()
    {
        return self::$params;
    } 

    /**
     * Sets the params to be used for requests.
     * @param string $params
     */
    private static function setParams($params)
    {
        self::$params = $params;
    }

    /**
     * @return string The params for the requests.
     */
    private static function getSignature()
    {
        return self::$signature;
    } 

    /**
     * Sets the params to be used for requests.
     * @param string $signature
     */
    private static function setSignature($signature)
    {
        self::$signature = $signature;
    }

    /**
     * @return string The final data for the requests.
     */
    private static function getRequestData()
    {
        return self::$requestData;
    } 

    /**
     * Sets the final data to be used for requests.
     * @param string $requestData
     */
    private static function setRequestData()
    {
        $requestData = self::getParams();
        $requestData['signature']  = self::getSignature();
        $requestData['merchantid']  = self::getMerchantId();
        self::$requestData = $requestData;
    }


    /**
     * @return string The transactionId for the requests.
     */
    private static function getTransactionId()
    {
        return self::$transactionId;
    } 

    /**
     * Sets the transactionId to be used for requests.
     *
     * @param string $transactionId
     */
    private static function setTransactionId($data)
    {
        self::$transactionId = $data;
    }

    /**
     * @return string The transactionId for the requests.
     */
    private static function getLanguage()
    {
        return self::$language;
    } 

    /**
     * Sets the transactionId to be used for requests.
     *
     * @param string $transactionId
     */
    private static function setLanguage($data)
    {
        self::$language = $data;
    }
    
    /**
     * @return string The final data for the requests.
     */
    private static function getResponseData()
    {
        return self::$responseData;
    } 

    /**
     * Sets the final data to be used for requests.
     *
     * @param string $responseData
     */
    private static function setResponseData($data)
    {
        self::$responseData = $data;
    }


    /**
     * @return string The final data for the requests.
     */
    private static function getAmount()
    {
        return self::$amount;
    } 

    /**
     * Sets the final data to be used for requests.
     *
     * @param string $responseData
     */
    private static function setAmount($data)
    {
        self::$amount = $data;
    }

        /**
     * @return string The final data for the requests.
     */
    private static function getCurrency()
    {
        return self::$currency;
    } 

    /**
     * Sets the final data to be used for requests.
     *
     * @param string $responseData
     */
    private static function setCurrency($data)
    {
        self::$currency = $data;
    }

    #########################
    ##   Cashier methods   ##
    #########################

    public static function create($merchantId, $secret, $url, CreditCardBuilder $params)
    {
        try {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: create / Message: Creating gateway...');

            self::setMerchantId(self::$merchantId = $merchantId);
            self::setSecret(self::$secret = $secret);
            self::setUrl(self::$url = $url);
            $params = (array) $params;
            $params['merchantid'] = $merchantId;
            self::setParams(self::$params = $params);

            $result = self::validateData(self::$merchantId, self::$secret, self::$url, 0);

            if(!empty($result)){
                return $result;
            }

            $sign = self::calculateSignature(self::$params,0);
            self::setSignature($sign);
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: create / Signature: '.$sign);

            self::setRequestData();    
            $response = self::doRequest(self::getRequestData());
            self::setResponseData($response);
            
            return $response;

        } catch (\Exception $e) {
            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: create / Message: '.$e->getMessage());
        }
        
    }

    public static function createHosted($merchantId, $secret, $url, HostedPageBuilder $params)
    {
        try {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: createHosted / Message: Creating gateway...');

            self::setMerchantId(self::$merchantId = $merchantId);
            self::setSecret(self::$secret = $secret);
            self::setUrl(self::$url = $url);
            $params = (array) $params;
            $params['merchantid'] = $merchantId;
            self::setParams(self::$params = $params);

            $result = self::validateData(self::$merchantId, self::$secret, self::$url, 0);

            if(!empty($result)){
                return $result;
            }
            
            $sign = self::calculateSignature(self::$params,0);
            self::setSignature($sign);
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: createHosted / Signature: '.$sign);

            self::setRequestData();    
            $response = self::doRequest(self::getRequestData());
            self::setResponseData($response);
            
            return $response;

        } catch (\Exception $e) {
            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: createHosted / Message: '.$e->getMessage());
        }
        
    }

    public static function createReversal($merchantId, $secret, $url, $transactionId, $language)
    {
        try {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: createReversal / Message: Creating gateway...');

            self::setMerchantId(self::$merchantId = $merchantId);
            self::setSecret(self::$secret = $secret);
            self::setUrl(self::$url = $url);
            self::setTransactionId(self::$transactionId = $transactionId);
            self::setLanguage(self::$language = $language);

            $params = array();
            $params['merchantid'] = $merchantId;
            $params['transactionid'] = $transactionId;
            $params['language'] = $language;
            self::setParams(self::$params = $params);

            $result = self::validateData(self::$merchantId, self::$secret, self::$url, 1);

            if(!empty($result)){
                return $result;
            }

            $sign = self::calculateSignature($params,0);
            self::setSignature($sign);

            $requestData['merchantid']  = self::getMerchantId();
            $requestData['signature']  = self::getSignature();
            $requestData['transactionid']  = self::getTransactionId();
            $requestData['language']  = self::getLanguage();

            $response = self::doRequest($requestData);
        
            return $response;

        } catch (\Exception $e) {
            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: createReversal / Message: '.$e->getMessage());
        }
        
    }

    public static function createRefund($merchantId, $secret, $url, $amount, $currency, $transactionId, $language, $externalid)
    {
        try {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: createRefund / Message: Creating gateway...');

            self::setMerchantId(self::$merchantId = $merchantId);
            self::setSecret(self::$secret = $secret);
            self::setUrl(self::$url = $url);
            self::setTransactionId(self::$transactionId = $transactionId);
            self::setLanguage(self::$language = $language);
            self::setAmount(self::$amount = $amount);
            self::setCurrency(self::$currency = $currency);

            $params = array();
            $params['merchantid'] = $merchantId;
            $params['amount'] = $amount;
            $params['currency'] = $currency;
            $params['transactionid'] = $transactionId;
            self::setParams(self::$params = $params);

            $result = self::validateData(self::$merchantId, self::$secret, self::$url, 2);

            if(!empty($result)){
                return $result;
            }

            $sign = self::calculateSignature($params);
            self::setSignature($sign);

            $requestData['merchantid']  = self::getMerchantId();
            $requestData['amount']  = self::getAmount();
            $requestData['currency']  = self::getCurrency();
            $requestData['signature']  = self::getSignature();
            $requestData['transactionid']  = self::getTransactionId();
            // $requestData['language']  = self::getLanguage();
            // $requestData['external_id']  = $externalid;

            $response = self::doRequest($requestData);
        
            return $response;

        } catch (\Exception $e) {
            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: createRefund / Message: '.$e->getMessage());
        }
        
    }

    
    public static function createCapture($merchantId, $secret, $url, $transactionId)
    {
        try {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: createCapture / Message: Creating gateway...');

            self::setMerchantId(self::$merchantId = $merchantId);
            self::setSecret(self::$secret = $secret);
            self::setUrl(self::$url = $url);
            self::setTransactionId(self::$transactionId = $transactionId);

            $result = self::validateData(self::$merchantId, self::$secret, self::$url, 3);

            if(!empty($result)){
                return $result;
            }

            $params = array();
            $params['merchantid'] = $merchantId;
            $params['transactionid'] = $transactionId;
            self::setParams(self::$params = $params);

            $sign = self::calculateSignature($params);
            self::setSignature($sign);

            $requestData['merchantid']  = self::getMerchantId();
            $requestData['signature']  = self::getSignature();
            $requestData['transactionid']  = self::getTransactionId();

            $response = self::doRequest($requestData);
        
            return $response;

        } catch (\Exception $e) {
            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: createCapture / Message: '.$e->getMessage());
        }
        
    }

    public static function createPartialCapture($merchantId, $secret, $url, $transactionId, $amount)
    {
        try {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: createCapture / Message: Creating gateway...');

            self::setMerchantId(self::$merchantId = $merchantId);
            self::setSecret(self::$secret = $secret);
            self::setUrl(self::$url = $url);
            self::setTransactionId(self::$transactionId = $transactionId);
            self::setAmount(self::$amount = $amount);

            $result = self::validateData(self::$merchantId, self::$secret, self::$url, 4);

            if(!empty($result)){
                return $result;
            }

            $params = array();
            $params['merchantid'] = $merchantId;
            $params['transactionid'] = $transactionId;
            $params['amount']  = $amount;
            self::setParams(self::$params = $params);

            $sign = self::calculateSignature($params);
            self::setSignature($sign);

            $requestData['merchantid']  = self::getMerchantId();
            $requestData['signature']  = self::getSignature();
            $requestData['transactionid']  = self::getTransactionId();
            $requestData['amount']  = self::getAmount();

            $response = self::doRequest($requestData);
        
            return $response;

        } catch (\Exception $e) {
            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: createCapture / Message: '.$e->getMessage());
        }
        
    }


    public static function createCFT_Transaction($merchantId, $secret, $url, $transactionId, $amount, $currency, $paymentMethod)
    {
        try {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: createCFT_Transaction / Message: Creating gateway...');

            self::setMerchantId(self::$merchantId = $merchantId);
            self::setSecret(self::$secret = $secret);
            self::setUrl(self::$url = $url);
            self::setTransactionId(self::$transactionId = $transactionId);
            self::setAmount(self::$amount = $amount);
            self::setCurrency(self::$currency = $currency);

            $result = self::validateData(self::$merchantId, self::$secret, self::$url, 5);

            if(!empty($result)){
                return $result;
            }

            $params = array();
            $params['merchantid'] = $merchantId;
            $params['transactionid'] = $transactionId;
            $params['amount']  = $amount;
            $params['currency']  = $currency;
            $params['payment_method']  = $paymentMethod;
            self::setParams(self::$params = $params);

            $sign = self::calculateSignature($params);
            self::setSignature($sign);

            $requestData['merchantid']  = self::getMerchantId();
            $requestData['amount']  = self::getAmount();
            $requestData['currency']  = self::getCurrency();
            $requestData['payment_method']  = $paymentMethod;
            $requestData['transactionid']  = self::getTransactionId();
            $requestData['signature']  = self::getSignature();

            $response = self::doRequest($requestData);
        
            return $response;

        } catch (\Exception $e) {
            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: createCFT_Transaction / Message: '.$e->getMessage());
        }
        
    }


    public static function createCFT_CreditCard($merchantId, $secret, $url, $amount, $currency, $paymentMethod, $cnn, $expMonth, $expYear, $cardholderName)
    {
        try {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: createCFT_CreditCard / Message: Creating gateway...');

            self::setMerchantId(self::$merchantId = $merchantId);
            self::setSecret(self::$secret = $secret);
            self::setUrl(self::$url = $url);
            self::setAmount(self::$amount = $amount);
            self::setCurrency(self::$currency = $currency);

            $result = self::validateData(self::$merchantId, self::$secret, self::$url, 0);

            if(!empty($result)){
                return $result;
            }
        
            $params = array();
            $params['merchantid'] = $merchantId;
            $params['amount']  = $amount;
            $params['currency']  = $currency;
            $params['payment_method']  = $paymentMethod;
            $params['cardholder_name']  = $cardholderName;
            $params['cnn']  = $cnn;
            $params['exp_month']  = $expMonth;
            $params['exp_year']  = $expYear;
            self::setParams(self::$params = $params);

            $sign = self::calculateSignature(self::$params);
            self::setSignature($sign);

            $requestData['merchantid']  = self::getMerchantId();
            $requestData['amount']  = self::getAmount();
            $requestData['currency']  = self::getCurrency();
            $requestData['payment_method']  = $paymentMethod;
            $requestData['signature']  = self::getSignature();
            $requestData['cardholder_name']  = $cardholderName;
            $requestData['cnn']  = $cnn;
            $requestData['exp_month']  = $expMonth;
            $requestData['exp_year']  = $expYear;

            $response = self::doRequest($requestData);
            self::setResponseData($response);
            
            return $response;

        } catch (\Exception $e) {
            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: createCFT_CreditCard / Message: '.$e->getMessage());
        }
        
    }

    public static function calculateSignature($data)
    {
        try {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: calculateSignature / Message: Creating signature...');

            $signature = ""; 
            ksort($data);
            foreach ( $data as $key => $value ){
                $signature .= $value;
            }
       
            $signature .= self::$secret;
            // var_dump($signature);
            return strtolower(sha1($signature));

        } catch (\Exception $e) {
            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: calculateSignature / Message: '.$e->getMessage());
        }
    }

    public static function doRequest($requestData)
    {
        try {
            
            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: doRequest / Message: Creating request...');
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: doRequest / Params: '.json_encode($requestData));
            
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($requestData)
                )
            );
            // var_dump($options);
            $context = stream_context_create($options);
            $result = file_get_contents(self::getUrl(), false, $context);
            
            return $result;

        } catch (\Exception $e) {
            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: doRequest / Message: '.$e->getMessage());
        }
    }


    public static function generateResult($response)
    {
        try {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: generateResult / Message: Generate result...');
            
            $array = explode("&", $response);
            foreach ($array as $value) {
                $item = explode("=", $value);
                $data[$item[0]] = $item[1];
            }

            if(isset($data['errormessage']) && $data['errormessage']!==''){
                $data['errormessage'] = urldecode($data['errormessage']);
            }

            if(isset($data['errmsg']) && $data['errmsg']!==''){
                $data['errmsg'] = urldecode($data['errmsg']);
            }

            if(isset($data['url_3ds']) && $data['url_3ds']!==''){
                $data['url_3ds'] = urldecode($data['url_3ds']);
            }

            if(isset($data['start_url']) && $data['start_url']!==''){
                $data['start_url'] = urldecode($data['start_url']);
            }

            return $data;

        } catch (\Exception $e) {
            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: calculateSignature / Message: '.$e->getMessage());
        }
    }

    
    #########################
    ## Cashier validations ##
    #########################


    public static function validateData($merchantId, $secret, $url, $type)
    {
        try {

            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: validateData / Message: Validating parameters...');
          
            if (empty($merchantId)) {
                $message = "Failed to pass el merchant ID parameter";
                $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: validateData / Message: '.$message);
                return $message;
            }

            if (empty($secret)) {
                $message = "Failed to pass secret parameter";
                $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: validateData / Message: '.$message);
                return $message;
            }

            if (empty($url)) {
                $message = "Failed to pass url parameter";
                $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: validateData / Message: '.$message);
                return $message;
            }
            

            switch ($type) {
                case 0://Authorize - Pre-authorize - CFT
                    $params = self::getParams();
                    if ($params && !is_array($params)) {
                        $message = "Failed to pass an array with the required parameters";
                        $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: validateData / Message: '.$message);
                        return $message;
                    }
                    break;
                case 1: //Reversal
                    $transid = self::getTransactionId();
                    $language = self::getLanguage();
                    if (empty($transid)) {
                        $message = "Failed to pass transaction id parameter";
                        $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: validateData / Message: '.$message);
                        return $message;
                    }
                    if (empty($language)) {
                        $message = "Failed to pass language parameter";
                        $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: validateData / Message: '.$message);
                        return $message;
                    }
                    break;
                case 2: //Refund
                    $amount = self::getAmount();
                    $currency = self::getCurrency();
                    if (empty($amount)) {
                        $message = "Failed to pass amount parameter";
                        $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: validateData / Message: '.$message);
                        return $message;
                    }
                    if (empty($currency)) {
                        $message = "Failed to pass currency parameter";
                        $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: validateData / Message: '.$message);
                        return $message;
                    }
                    break;
                case 3: //Capture
                    $transid = self::getTransactionId();
                    if (empty($transid)) {
                        $message = "Failed to pass transaction id parameter";
                        $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: validateData / Message: '.$message);
                        return $message;
                    }
                    break;
                case 4: //Partial Capture
                    $amount = self::getAmount();
                    $transid = self::getTransactionId();
                    if (empty($transid)) {
                        $message = "Failed to pass transaction id parameter";
                        $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: validateData / Message: '.$message);
                        return $message;
                    }
                    if (empty($amount)) {
                        $message = "Failed to pass amount parameter";
                        $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: validateData / Message: '.$message);
                        return $message;
                    }
                    break;
                case 5: //CFT Transaction
                    $amount = self::getAmount();
                    $transid = self::getTransactionId();
                    $currency = self::getCurrency();
                    if (empty($transid)) {
                        $message = "Failed to pass transaction id parameter";
                        $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: validateData / Message: '.$message);
                        return $message;
                    }
                    if (empty($amount)) {
                        $message = "Failed to pass amount parameter";
                        $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: validateData / Message: '.$message);
                        return $message;
                    }
                    if (empty($currency)) {
                        $message = "Failed to pass currency parameter";
                        $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: validateData / Message: '.$message);
                        return $message;
                    }
                    break;
            }

            
        } catch (\Exception $e) {
            $loggingService = new LoggingService();
            $loggingService->logInfo('2', 'Cashier.php / Payment method: credit-card / Function: validateData / Message: '.$e->getMessage());
        }
    }

}



?>