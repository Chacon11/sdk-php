<?php
namespace payabl\service;

class LoggingService
{
    
    public function __construct(){}

    /**
     * Log data based on type
     */
    public function logInfo($type,$msg)
    {
        
        try {  

            $file='info.log';

            switch($type){
				case 1: $file='info.log'; break;
				case 2: $file='creditCard.log'; break;
				default:$file='info.log';
			}
			
            
			$path=__DIR__.'/../log/';

			$logFile = $path.date('Y-m-d').'.'.$file;
	 		
	 		$fh = fopen($logFile, 'a') or die("can't open file");  

	 		$date=date("Y-m-d H:i:s");
	 		
	        fwrite($fh, "\n".$date."   ");  
	   		fwrite($fh,$file.' '.$msg);
			fclose($fh);

        }catch (\Exception $exception) {
            echo $exception->getMessage();exit;  
        }
       
    }

}