<?php

include __DIR__."/../vendor/autoload.php";
use \payabl\model\CreditCardBuilder;
use payabl\creditCard\creditCardPayment;

//Payment method instance (merchantID, secret, baseUrl)
$creditCardPayment = new CreditCardPayment('demo_shop', 'b185', 'https://sandbox.powercash21.com');

//Object instance
$creditCardBuilder = new CreditCardBuilder;

$creditCardBuilder->amount('1.23');
$creditCardBuilder->orderId('Powercash21-Test');
$creditCardBuilder->cardholderName('Muster Mann');
$creditCardBuilder->ccn('4242424242424242');
$creditCardBuilder->cvcCode('123');
$creditCardBuilder->expMonth('12');
$creditCardBuilder->expYear('2030');
$creditCardBuilder->company('Powerpay21');
$creditCardBuilder->currency('USD');
$creditCardBuilder->customerip('127.0.0.1');
$creditCardBuilder->email('test@gmail.com');
$creditCardBuilder->firstName('Muster');
$creditCardBuilder->lastName('Mann');
$creditCardBuilder->paymentMethod('1');
$creditCardBuilder->country('DEU');
$creditCardBuilder->city('Frankfurt');
$creditCardBuilder->street('Hanauer Landstrasse');
$creditCardBuilder->zip('60322');
$creditCardBuilder->language('en');
$creditCardBuilder->gender('M');
$creditCardBuilder->salutation('Herr');
$creditCardBuilder->company('Powercash21');
$creditCardBuilder->birthday('07071971');
$creditCardBuilder->house('19');
$creditCardBuilder->state('HE');
$creditCardBuilder->email('test@inatec.com');
$creditCardBuilder->phone('00496975938501');
$creditCardBuilder->notificationUrl('http://localhost/url_notification');
// $creditCardBuilder->urlReturn('http://localhost/url_return');

// $creditCardBuilder->urlFailed('http://localhost/url_failed');
// $creditCardBuilder->urlSuccess('http://localhost/url_success');


$result = $creditCardBuilder->build();


//Required inputs validations
if(empty($result['errors'])){

####################
##   Authorize    ##
####################

//--->(CreditCardBuilder)

// $creditCardPayment->authorize($creditCardBuilder); 
// $authorize = json_decode($creditCardPayment->response);
// var_dump('Authorize', $creditCardPayment);

####################
##  Pre-Authorize ##
####################

//--->(CreditCardBuilder)

// $creditCardPayment->preAuthorize($creditCardBuilder); 
// $preAuthorize = json_decode($creditCardPayment->response);
// var_dump('Pre-Authorize', $creditCardPayment);


####################
##    Reversal    ##
####################

//Use authorize or pre-authorize --->(transactionid, language)

// $creditCardPayment->reversal('104153535','en'); 
// $reversal = json_decode($creditCardPayment->response);
// var_dump('Reversal',$creditCardPayment);


####################
##     Refund     ##
####################

//Use authorize --->(amount, currency, transactionid, language (optional), external_id (optional) )

// $creditCardPayment->refund('1.23', 'USD', '104153543'); 
// $refund = json_decode($creditCardPayment->response);
// var_dump('Refund',$creditCardPayment);


####################
##    Capture     ##
####################

//Use pre-authorize --->(transactionid)

// $creditCardPayment->capture('104153554'); 
// $capture = json_decode($creditCardPayment->response);
// var_dump('Capture',$creditCardPayment);



#####################
## Partial Capture ##
#####################

//Use pre-authorize --->(transactionid, amount)

// $creditCardPayment->partialCapture('104153560', '1.00'); 
// $capture = json_decode($creditCardPayment->response);
// var_dump('Partial Capture',$creditCardPayment);


########################
# Credit Fund Transfer #
########################

//Use authorize --->(transactionid, amount, currency, payment_method = 1)

// $creditCardPayment->cft_transaction('104153613', '1.00', 'USD'); 
// $cft = json_decode($creditCardPayment->response);
// var_dump('CFT Transaction',$creditCardPayment);


//--->(amount, currency, payment_method, cnn, exp_month, exp_year, cardholder_name)

// $creditCardPayment->cft_creditCard('1.00',' USD', '1', '4242424242424242', '12','2030','Muster Mann'); 
// $capture = json_decode($creditCardPayment->response);
// var_dump('CFT CreditCard',$creditCardPayment);


}else{
 var_dump($result);
}