<?php

include __DIR__."/../vendor/autoload.php";
use \payabl\model\CreditCardBuilder;
use payabl\creditCard\WidgetPayment;

//Payment method instance (merchantID, secret, baseUrl)
$widgetPayment = new WidgetPayment('demo-shop', 'b185', 'https://sandbox.powercash21.com');

//Object instance
$creditCardBuilder = new CreditCardBuilder;
$creditCardBuilder->amount('1.23');
$creditCardBuilder->currency('USD');
// $creditCardBuilder->shopUrl('http://localhost/shop_url');
// $creditCardBuilder->email('chrischacon11@gmail.com');
// $creditCardBuilder->customerip('127.0.0.1');

$result = $creditCardBuilder->build($widgetPayment);

//Required inputs validations
if(empty($result['errors'])){ 

###############
##   Start   ##
###############

//--->(CreditCardBuilder)

$widgetPayment->start($creditCardBuilder); 
$start = json_decode($widgetPayment->response);
var_dump('Widget Payment', $widgetPayment);


}else{
 var_dump($result);
}