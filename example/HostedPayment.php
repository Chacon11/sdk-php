<?php

include __DIR__."/../vendor/autoload.php";
use \payabl\model\HostedPageBuilder;
use payabl\creditCard\HostedPayment;

//Payment method instance (merchantID, secret, baseUrl)
$hostedPayment = new HostedPayment('demo_shop', 'b185', 'https://sandbox.powercash21.com');

//Object instance
$hostedPageBuilder = new HostedPageBuilder;
$hostedPageBuilder->amount('1.23');
$hostedPageBuilder->currency('EUR');
$hostedPageBuilder->language('en');
$hostedPageBuilder->gender('M');
$hostedPageBuilder->firstName('Max');
$hostedPageBuilder->lastName('Mustermann');
$hostedPageBuilder->street('Bockenheimer');
$hostedPageBuilder->zip('60323');
$hostedPageBuilder->city('Frankfurt');
$hostedPageBuilder->country('DEU');
$hostedPageBuilder->email('test@gmail.com');
$hostedPageBuilder->customerip('127.0.0.1');
$hostedPageBuilder->company('Powerpay21');
$hostedPageBuilder->state('Hessen');
$hostedPageBuilder->salutation('Herr');
$hostedPageBuilder->title('Dr');
$hostedPageBuilder->birthday('01011970');
$hostedPageBuilder->house('123');
$hostedPageBuilder->phone('069777770');
$hostedPageBuilder->fax('0697777715');
$hostedPageBuilder->mobile('01797777777');
$hostedPageBuilder->custom1('12345');
$hostedPageBuilder->paymentMethod('1');
$hostedPageBuilder->urlReturn('http://localhost/url_return');

$result = $hostedPageBuilder->build();

//Required inputs validations
if(empty($result['errors'])){ 

###############
##   Start   ##
###############

//--->(hostedPageBuilder)

$hostedPayment->start($hostedPageBuilder); 
$start = json_decode($hostedPayment->response);
var_dump('Hosted Payment', $hostedPayment);


}else{
 var_dump($result);
}